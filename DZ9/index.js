let list = function (masiv, r = "document.body") {
  let parent = document.createElement(`r`);
  let ul = document.createElement("ul");

  document.body.append(parent);
  parent.append(ul);

  for (let i of masiv) {
    let li = document.createElement("li");
    li.innerHTML = `${i}`;
    ul.append(li);
  }
};

let goroda = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

list(goroda, "div");
