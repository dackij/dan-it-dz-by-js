let par = document.getElementsByTagName("p");
for (let p of par) {
  p.style.background = "#ff0000";
}

let opt = document.getElementById("optionsList");
console.log(opt);
let father = opt.parentNode;
console.log(father);
let child = opt.children;
for (let chi of child) {
  console.log(chi);
}

let test = document.getElementById("testParagraph");
test.textContent = "This is a paragraph";

let header = document.querySelector(".main-header");
let item = header.querySelectorAll("*");
console.log(item);
for (let i of item) {
  i.className = "nav-item";
}

let title = document.querySelectorAll(".section-title");
for(let t of title){
	t.classList.remove("section-title")
	console.log(t);
}
