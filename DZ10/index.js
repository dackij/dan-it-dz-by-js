let tab = function () {
  let tabs = document.querySelectorAll(".tabs-title");
  let tabsContent = document.querySelectorAll(".content");
  let tabName;

  for (let i = 0; i < tabs.length; i++) {
    tabs[i].addEventListener("click", selectTab);
  }

  function selectTab() {
    for (let i = 0; i < tabs.length; i++) {
      tabs[i].classList.remove("active");
    }
    this.classList.add("active");
    tabName = this.dataset.tabName;
    console.log(tabName);
    selectContent(tabName);
  }

  function selectContent(tabName) {
    for (let i = 0; i < tabsContent.length; i++) {
      if (tabsContent[i].classList.contains(tabName)) {
        tabsContent[i].classList.add("show");
      } else {
        tabsContent[i].classList.remove("show");
      }
    }
  }
};
tab();
