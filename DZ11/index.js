let showPassword = document.querySelectorAll(".icon-password");
let btn = document.querySelector(".btn");
let pasFirst = document.querySelector(".password-first");
let pasSecond = document.querySelector(".password-second");
let message = document.querySelector(".message");

showPassword.forEach((item) => item.addEventListener("click", toggleType));

function toggleType() {
  let input = this.closest(".input-wrapper").querySelector(".password");

  if (input.type === "password") {
    input.type = "text";
    this.classList.add("fa-eye-slash");
  } else {
    input.type = "password";
    this.classList.remove("fa-eye-slash");
  }
}

btn.addEventListener("click", pasCompr);

function pasCompr() {
  if (pasFirst.value === pasSecond.value) {
    alert("You are welcome!");
  } else {
    message.innerHTML = "Потрібно ввести однакові значення";
  }
}
