// Tabs

let tab = function () {
  let tabs = document.querySelectorAll(".service-menu-style");
  let tabsContent = document.querySelectorAll(".service-text");
  let tabName;

  for (let i = 0; i < tabs.length; i++) {
    tabs[i].addEventListener("click", selectTab);
  }

  function selectTab() {
    for (let i = 0; i < tabs.length; i++) {
      tabs[i].classList.remove("menu-activ");
    }
    this.classList.add("menu-activ");
    tabName = this.dataset.tabName;
    selectContent(tabName);
  }

  function selectContent(tabName) {
    for (let i = 0; i < tabsContent.length; i++) {
      if (tabsContent[i].classList.contains(tabName)) {
        tabsContent[i].classList.add("show");
      } else {
        tabsContent[i].classList.remove("show");
      }
    }
  }
};
tab();

// Load More

const loadMore = document.querySelector(".work-btn-style");
const imgLenrth = document.querySelectorAll(".work-img-style").length;
let items = 12;

loadMore.addEventListener("click", () => {
  items += 12;
  const array = Array.from(document.querySelector(".work-img").children);
  const visItems = array.slice(0, items);
  visItems.forEach((el) => el.classList.add("visible"));

  if (visItems.length === imgLenrth + 1) {
    loadMore.style.display = "none";
  }
});

// Filter

const workMenu = document.querySelector(".work-menu");
let workImg = document.querySelectorAll(".work-img-style");
let workMenuStyle = document.querySelectorAll(".work-menu-style");

function filter() {
  workMenu.addEventListener("click", (e) => {
    const targetId = e.target.dataset.id;
    const target = e.target;

    workMenuStyle.forEach((listItem) =>
      listItem.classList.remove("work-activ")
    );
    target.classList.add("work-activ");

    switch (targetId) {
      case "all":
        getItems("work-img-style");
        break;
      case "graphic-design":
        getItems(targetId);
        break;
      case "web-design":
        getItems(targetId);
        break;
      case "landing-pages":
        getItems(targetId);
        break;
      case "wordpress":
        getItems(targetId);
        break;
    }
  });
}
filter();

function getItems(className) {
  workImg.forEach((item) => {
    if (
      (item.classList.contains(className) &&
        item.classList.contains("first")) ||
      (item.classList.contains(className) && item.classList.contains("visible"))
    ) {
      item.style.display = "block";
    } else {
      item.style.display = "none";
    }
  });
}

// Slaider

let offset = 0;
const feedbackInfoSlaider = document.querySelector(".feedback-info-slaider");
let feedbackImgSmal = document.querySelectorAll(".feedback-img-smal");

document.querySelector(".left").addEventListener("click", function () {
  offset -= 1160;
  if (offset < -3480) {
    offset = 0;
  }
  feedbackInfoSlaider.style.left = offset + "px";
  switch (offset) {
    case 0:
      for (let i = 0; i < feedbackImgSmal.length; i++) {
        feedbackImgSmal[i].classList.remove("activ-feedback");
      }
      feedbackImgSmal[0].classList.add("activ-feedback");
      break;
    case -1160:
      for (let i = 0; i < feedbackImgSmal.length; i++) {
        feedbackImgSmal[i].classList.remove("activ-feedback");
      }
      feedbackImgSmal[1].classList.add("activ-feedback");
      break;
    case -2320:
      for (let i = 0; i < feedbackImgSmal.length; i++) {
        feedbackImgSmal[i].classList.remove("activ-feedback");
      }
      feedbackImgSmal[2].classList.add("activ-feedback");
      break;
    case -3480:
      for (let i = 0; i < feedbackImgSmal.length; i++) {
        feedbackImgSmal[i].classList.remove("activ-feedback");
      }
      feedbackImgSmal[3].classList.add("activ-feedback");
      break;
  }
});

document.querySelector(".right").addEventListener("click", function () {
  offset += 1160;
  if (offset > 0) {
    offset = -3480;
  }
  feedbackInfoSlaider.style.left = offset + "px";
  switch (offset) {
    case 0:
      for (let i = 0; i < feedbackImgSmal.length; i++) {
        feedbackImgSmal[i].classList.remove("activ-feedback");
      }
      feedbackImgSmal[0].classList.add("activ-feedback");
      break;
    case -1160:
      for (let i = 0; i < feedbackImgSmal.length; i++) {
        feedbackImgSmal[i].classList.remove("activ-feedback");
      }
      feedbackImgSmal[1].classList.add("activ-feedback");
      break;
    case -2320:
      for (let i = 0; i < feedbackImgSmal.length; i++) {
        feedbackImgSmal[i].classList.remove("activ-feedback");
      }
      feedbackImgSmal[2].classList.add("activ-feedback");
      break;
    case -3480:
      for (let i = 0; i < feedbackImgSmal.length; i++) {
        feedbackImgSmal[i].classList.remove("activ-feedback");
      }
      feedbackImgSmal[3].classList.add("activ-feedback");
      break;
  }
});

for (let i = 0; i < feedbackImgSmal.length; i++) {
  feedbackImgSmal[i].addEventListener("click", function () {
    for (let i = 0; i < feedbackImgSmal.length; i++) {
      feedbackImgSmal[i].classList.remove("activ-feedback");
    }
    this.classList.add("activ-feedback");
    switch (i) {
      case 0:
        offset = 0;
        feedbackInfoSlaider.style.left = offset + "px";
        break;
      case 1:
        offset = -1160;
        feedbackInfoSlaider.style.left = offset + "px";
        break;
      case 2:
        offset = -2320;
        feedbackInfoSlaider.style.left = offset + "px";
        break;
      case 3:
        offset = -3480;
        feedbackInfoSlaider.style.left = offset + "px";
        break;
    }
  });
}
